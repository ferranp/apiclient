import datetime
import io
import json
from urllib.parse import parse_qs, urlencode, urlparse

import requests
from django.core.serializers.json import DjangoJSONEncoder
from django.utils.translation import get_language

TIMEOUT = 10


class ApiException(Exception):
    def __init__(self, status, text, json=None):
        super().__init__("%s %s" % (status, text))
        self.status = status
        self.text = text
        self.json = json


class Client(object):
    def __init__(self, base_url, token=None):
        self.base_url = base_url
        self.token = token

    def url(self, resource, params=None):
        if resource.startswith("http:") or resource.startswith("https:"):
            url_ = resource
        else:
            url_ = "%s%s/" % (self.base_url, resource)

        if params:
            url_ = "%s?%s" % (url_, urlencode(params))

        return url_

    def request_headers(self, json=False):
        headers = {
            "Accept-Language": get_language(),
        }
        if self.token:
            headers["Authorization"] = "Token %s" % self.token
        if json:
            headers["Content-Type"] = "application/json"

        return headers

    def get(self, resource, params=None, timeout=TIMEOUT):
        r = requests.get(
            self.url(resource, params), headers=self.request_headers(), timeout=timeout
        )
        if r.status_code == 200:
            items = r.json()
            return items
        else:
            raise ApiException(status=r.status_code, text=r.text)

    def get_all_results(self, resource, params=None, timeout=TIMEOUT):

        all_results = []

        results = self.get(resource, params=params, timeout=timeout)

        if results and results["results"]:
            all_results = results["results"]

        while results["next"]:
            results = self.get(results["next"], timeout=timeout)
            all_results = all_results + results["results"]

        return all_results

    def getraw(self, resource, params=None, timeout=TIMEOUT):
        return requests.get(
            self.url(resource, params), headers=self.request_headers(), timeout=timeout
        )

    def put(self, resource, data, params=None):
        r = requests.put(
            self.url(resource, params),
            data=json.dumps(data, cls=DjangoJSONEncoder),
            headers=self.request_headers(json=True),
        )
        if r.status_code == 200:
            items = r.json()
            return items
        else:
            raise ApiException(status=r.status_code, text=r.text)

    def post(self, resource, data, params=None):
        r = requests.post(
            self.url(resource, params),
            data=json.dumps(data, cls=DjangoJSONEncoder),
            headers=self.request_headers(json=True),
        )
        if r.status_code in (200, 201):
            item = r.json()
            return item
        else:
            raise ApiException(status=r.status_code, text=r.text, json=r.json())

    def postraw(self, resource, data, params=None, name="file"):
        headers = self.request_headers()
        file = io.BytesIO(data)
        r = requests.post(
            self.url(resource, params), files={name: file}, headers=headers
        )

        if r.status_code in (200, 201):
            item = r.json()
            return item
        else:
            raise ApiException(status=r.status_code, text=r.text)

    def patch(self, resource, data, params=None):
        r = requests.patch(
            self.url(resource, params),
            data=json.dumps(data, cls=DjangoJSONEncoder),
            headers=self.request_headers(json=True),
        )
        if r.status_code in (200, 201):
            item = r.json()
            return item
        elif r.status_code == 202:
            return None
        else:
            raise ApiException(status=r.status_code, text=r.text)

    def filter(self, resource, request, defaults=None, forced=None, timeout=TIMEOUT):
        if not defaults:
            defaults = {}

        params = defaults.copy()

        for key in request.GET.keys():
            params[key] = request.GET[key]

        if forced:
            for key in forced:
                params[key] = forced[key]

        data = self.get(resource, params, timeout=timeout)
        if not data:
            data = {}

        page = {"current": params.get("page", 1)}

        if data.get("next"):
            url = urlparse(data["next"])
            page["next"] = url.query
            page["has_next"] = True

            qs = parse_qs(url.query)
            qs["page"] = "last"
            page["last"] = urlencode(qs)
            qs["page"] = "1"
            page["first"] = urlencode(qs)

        if data.get("previous"):
            url = urlparse(data["previous"])
            page["previous"] = url.query
            page["has_previous"] = True

            qs = parse_qs(url.query)
            qs["page"] = "last"
            page["last"] = urlencode(qs)
            qs["page"] = "1"
            page["first"] = urlencode(qs)

        data["page"] = page

        return data
